package com.example.redditek

import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class DefaultPostGetter {

    companion object{

        protected var userName : String = ""
        protected var userDescription : String = ""
        protected var userImage : String = ""

        fun getMyUserName(): String{
            val sortie =  this.userName
            return sortie
        }

        fun getMyUserDescription(): String{
            val sortie =  this.userDescription
            return sortie
        }

        fun getMyUserImage(): String{
            val sortie =  this.userImage
            return sortie
        }

        fun getDefaultPost(token:String) {
            val client = OkHttpClient()
            val request = Request.Builder()
                .addHeader("User-Agent", "android:com.example.redditek:v1 (by /u/gsorinEpitech)")
                .addHeader("Authorization","Bearer "+token)
                .url("https://oauth.reddit.com/subreddits/default")
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response){
                    val jsonObject = JSONObject(response.body()?.string().toString())
                    //println("DefaultPost : "+JSONObject(response.body()?.string().toString()))

                    var moreData = jsonObject.getString("data")
                    val jsonObjectMoreData = JSONObject(moreData)
                    println(moreData)

                    var moremoreData =jsonObjectMoreData.getJSONObject("children")
                    //var moremoreData = jsonObjectMoreData.getString("children")
                    println(moremoreData)
                    /*val jsonObjectMoremoreData = JSONObject(moremoreData)

                    var moremoremoreData = jsonObjectMoremoreData.getString("data")
                    val jsonObjectMoremoremoreData = JSONObject(moremoremoreData)*/

                    /*var gettingUserName =jsonObject.getString("display_name")
                    println("userName :"+gettingUserName)*/

                    /*//Récupération du nom du User
                    var gettingUserName =jsonObject.getString("name")
                    println("userName :"+gettingUserName)
                    //Récupération de la description
                    var gettingUserDesc =jsonObjectMoreData.getString("public_description")
                    println("userDescription :"+gettingUserDesc)
                    // Récupération de l'icone
                    var gettingUserIcon =jsonObjectMoreData.getString("icon_img")
                    println("userImage :"+gettingUserIcon)

                    userName=gettingUserName
                    userDescription=gettingUserDesc
                    userImage=gettingUserIcon*/
                }
            })
        }
    }
}