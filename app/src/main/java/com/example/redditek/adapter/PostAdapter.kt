package com.example.redditek.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.redditek.R
import com.example.redditek.dommain.PostReddit
import com.squareup.picasso.Picasso

class PostAdapter(private val dataSet: Array<PostReddit>) :
    RecyclerView.Adapter<PostAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val postAuthorTV: TextView = view.findViewById(R.id.post_author)
        val postTitleTV: TextView = view.findViewById(R.id.post_title)
        val postImageIV: ImageView = view.findViewById(R.id.post_image)
        val postTextTV: TextView = view.findViewById(R.id.post_text)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_item_post, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        viewHolder.postAuthorTV.text = dataSet[position].author
        viewHolder.postTitleTV.text = dataSet[position].title
        viewHolder.postTextTV.text = dataSet[position].text
        if (!dataSet[position].img.isEmpty()){
            Picasso.get().load(dataSet[position].img).into(viewHolder.postImageIV)
        }
    }

    override fun getItemCount() = dataSet.size

}