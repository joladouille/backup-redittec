package com.example.redditek

import android.icu.text.CaseMap.Title
import android.os.Bundle
import android.provider.ContactsContract.CommonDataKinds.Email
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.example.redditek.databinding.ActivityMainBinding
import com.google.android.material.navigation.NavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    val bindingInflater: (LayoutInflater) -> ActivityMainBinding =
        ActivityMainBinding::inflate

    lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = bindingInflater(layoutInflater)

        setContentView(binding.root)


        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home//, R.id.nav_camera
            ), drawerLayout
        )
        navView.setupWithNavController(navController)


        navView.invalidate()


        val headerView: View =
            LayoutInflater.from(this).inflate(R.layout.nav_header_main, navView, false)


        var profilPicture = findViewById<ImageView>(R.id.profil_pickture_d_IV)
        var profilName: TextView = headerView.findViewById<TextView>(R.id.name_TV)
        var profilMail: TextView = headerView.findViewById<TextView>(R.id.profil_email_TV)

        profilName.text = "jean-michel"
        profilMail.text = "jean-michel@gmiel.com"


    }
}
