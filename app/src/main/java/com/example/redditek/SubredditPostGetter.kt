package com.example.redditek

import android.content.ContentValues.TAG
import android.util.Log
import android.util.Log.ASSERT
import android.util.Log.INFO
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class SubredditPostGetter {

    companion object{

        var authorList = ArrayList<String>()
        var titleList = ArrayList<String>()
        var imgList = ArrayList<String>()
        var textList = ArrayList<String>()
        var SubredditPostListLenght= 0

        fun SubredditPostListLenght(): Int{
            val sortie =  this.SubredditPostListLenght
            return sortie
        }

        fun resetSubredditPostListLenght(){
            this.SubredditPostListLenght=0
        }

        fun getauthorList(): ArrayList<String>{
            val sortie =  this.authorList
            return sortie
        }
        fun resetAuthorList(){
            this.authorList = ArrayList<String>()
        }

        fun gettitleList(): ArrayList<String>{
            val sortie =  this.titleList
            return sortie
        }
        fun resetTitleList(){
            this.titleList = ArrayList<String>()
        }

        fun getimgList(): ArrayList<String>{
            val sortie =  this.imgList
            return sortie
        }
        fun resetImgList(){
            this.imgList = ArrayList<String>()
        }

        fun gettextList(): ArrayList<String>{
            val sortie =  this.textList
            return sortie
        }
        fun resetTextList(){
            this.textList = ArrayList<String>()
        }


        fun getSubredditPost(token:String,subredditGiven:String,sortingGiven:String) {
            var sortingGiven2= sortingGiven
            if(sortingGiven==null || sortingGiven.equals("") || sortingGiven==""){
                sortingGiven2="random"
            }
            val client = OkHttpClient()
            val request = Request.Builder()
                .addHeader("User-Agent", "android:com.example.redditek:v1 (by /u/gsorinEpitech)")
                .addHeader("Authorization","Bearer "+token)
                .url("https://oauth.reddit.com/r/"+subredditGiven+"/"+sortingGiven2+"/.json?limit=6")
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response){
                    val jsonObject = JSONObject(response.body()?.string().toString())

                    var jsonObject2 = jsonObject.getJSONObject("data")
                    var jsonObject3= jsonObject2.getJSONArray("children")


                    var childrenLength = jsonObject3.length()
                    SubredditPostListLenght = childrenLength

                    var i =0
                    while(i<childrenLength){
                        println("Json object : "+jsonObject3[i])

                        if(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("selftext")==""&&JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("media_embed")=="{}"){

                            if(JSONObject(jsonObject3[i].toString()).getString("data").contains("is_gallery")){
                                SubredditPostListLenght -= 1
                            }else{
                                authorList.add(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("author"))
                                titleList.add(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("title"))
                                textList.add("")
                                imgList.add(JSONObject(JSONObject(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("preview")).getJSONArray("images")[0].toString()).getJSONObject("source").getString("url"))
                            }

                        }else if(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("media_embed")!="{}"){

                            authorList.add(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("author"))
                            titleList.add(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("title"))
                            imgList.add("empty")
                            textList.add("Video : "+JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getJSONObject("media_embed").getString("content"))
                        }else{

                            authorList.add(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("author"))
                            titleList.add(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("title"))
                            imgList.add("empty")
                            textList.add(JSONObject(JSONObject(jsonObject3[i].toString()).getString("data")).getString("selftext"))
                        }
                        i++
                    }
                    i=0

                }
            })
        }
    }
}