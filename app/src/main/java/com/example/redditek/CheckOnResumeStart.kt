package com.example.redditek

class CheckOnResumeStart {


    companion object {
        var start = true

        fun isStartTrue(): Boolean {
            val sortie = this.start
            this.start = false
            return sortie
        }
    }
}