package com.example.redditek.ui.login

import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.redditek.CheckOnResumeStart
import com.example.redditek.R
import com.example.redditek.TockenGetter.Companion.accessToken
import com.example.redditek.TockenGetter.Companion.getAccessToken
import com.example.redditek.TockenGetter.Companion.getMyAccessToken
import com.example.redditek.databinding.FragmentLoginBinding
import kotlinx.coroutines.delay


class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding

    private val AUTH_URL = "https://www.reddit.com/api/v1/authorize.compact?client_id=%s" +
            "&response_type=code&state=%s&redirect_uri=%s&" +
            "duration=permanent&scope=identity read account edit flair history modconfig modflair modlog modposts modwiki mysubreddits report save submit subscribe vote wikiedit wikiread"

    private val CLIENT_ID = "mFgWyX5FA03ozeTfIqGNgQ"

    private val REDIRECT_URI = "http://www.testingoauth2.com/my_redirect"

    private val STATE = "MY_RANDOM_STRING_1"




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginButton.setOnClickListener{
            val url = String.format(AUTH_URL, CLIENT_ID, STATE, REDIRECT_URI)
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }


    }

    override fun onResume() {
        super.onResume()
        //println("fragment intent data : " + activity?.intent?.data)
        val uri = activity?.intent?.data
        val code = uri?.getQueryParameter("code")
        //println("fragment code : "+code)
        getAccessToken(code)
        var token = getMyAccessToken().toString()

        var isStartBool = CheckOnResumeStart.isStartTrue()
        //println("isStartBool : "+isStartBool)

        while(!isStartBool && token == ""){
            token = getMyAccessToken().toString()
            //println("token : " +token)
        }

        if(token!=""){
            view?.findNavController()?.navigate(R.id.action_login_to_home)
        }

    }
}