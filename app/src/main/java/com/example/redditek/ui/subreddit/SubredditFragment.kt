package com.example.redditek.ui.subreddit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.redditek.*
import com.example.redditek.databinding.FragmentSubredditBinding
import com.example.redditek.adapter.PostAdapter
import com.example.redditek.dommain.PostReddit
import com.example.redditek.dommain.Subreddit
import com.squareup.picasso.Picasso


class SubredditFragment : Fragment() {

    private lateinit var binding : FragmentSubredditBinding

    //TODO: remove test
    private var PostRedditList: Array<PostReddit> = Array(1) { PostReddit("","","","") }

    var subredditGiven : String = "Cat"
    var subredditState : String = "hot"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSubredditBinding.inflate(inflater, container, false)

        binding.listSubredditSub.layoutManager = LinearLayoutManager(activity)
        binding.listSubredditSub.adapter = PostAdapter(PostRedditList)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subredditGiven=getQuerySearch.getquery()

        while(subredditGiven==""){
            subredditGiven=getQuerySearch.getquery()
        }
        println("querygiven : "+subredditGiven)

        SubredditDataGetter.resetsubredditId()
        SubredditDataGetter.resetsubredditDescription()
        SubredditDataGetter.resetsubredditImage()
        SubredditDataGetter.resetsubredditImageBackgroud()
        SubredditDataGetter.resetsubredditName()
        SubredditDataGetter.resetsubredditSubscribers()
        SubredditDataGetter.resetsubredditTitle()
        SubredditDataGetter.resetuserSubscribed()
        SubredditPostGetter.resetSubredditPostListLenght()
        SubredditPostGetter.resetAuthorList()
        SubredditPostGetter.resetTitleList()
        SubredditPostGetter.resetImgList()
        SubredditPostGetter.resetTextList()



        var token = TockenGetter.getMyAccessToken().toString()
        SubredditDataGetter.getSubredditData(token,subredditGiven)

        var nameSubreddit = SubredditDataGetter.getsubredditName()
        while(nameSubreddit==""||nameSubreddit==null){
            nameSubreddit= SubredditDataGetter.getsubredditName()
        }
        println("nameSubreddit : "+nameSubreddit)
        binding.subredditNameTV.text=nameSubreddit

        var descSubreddit = SubredditDataGetter.getsubredditDescription()
        while(descSubreddit==""||descSubreddit==null){
            descSubreddit= SubredditDataGetter.getsubredditDescription()
        }
        binding.subDescTV.text=descSubreddit

        var fullNameSubreddit = SubredditDataGetter.getsubredditTitle()
        while(fullNameSubreddit==""||fullNameSubreddit==null){
            fullNameSubreddit= SubredditDataGetter.getsubredditTitle()
        }
        binding.subredditTitleTV.text = fullNameSubreddit

        var Suscribers = SubredditDataGetter.getsubredditSubscribers()
        while(Suscribers==""||Suscribers==null){
            Suscribers= SubredditDataGetter.getsubredditSubscribers()
        }
        binding.followersSubTV.text = Suscribers+" followers"

        if(SubredditDataGetter.getuserSubscribed()=="true"){
            binding.joinBtn.text = "Unsubscribe"
        }else{
            binding.joinBtn.text = "Subscribe"
        }

        var subredditImg = SubredditDataGetter.getsubredditImage()
        while(subredditImg==""||subredditImg==null){
            subredditImg= SubredditDataGetter.getsubredditImage()
        }

        var subredditBackground = SubredditDataGetter.getsubredditImageBackgroud()
        while(subredditBackground==""||subredditBackground==null){
            subredditBackground= SubredditDataGetter.getsubredditImageBackgroud()
        }

        Picasso.get().load(subredditImg.replace("amp;","")).into(binding.subPictureIV)
        Picasso.get().load(subredditBackground.replace("amp;","")).into(binding.subBackgroundIV)


        SubredditPostGetter.getSubredditPost(token, subredditGiven, subredditState)
        var dataListLength = SubredditPostGetter.SubredditPostListLenght
        while(dataListLength==0){
            dataListLength = SubredditPostGetter.SubredditPostListLenght
        }

        var authorList = SubredditPostGetter.getauthorList()
        var titleList = SubredditPostGetter.gettitleList()
        var imgList = SubredditPostGetter.getimgList()
        var textList = SubredditPostGetter.gettextList()

        var authorListSize = authorList.size
        var titleListSize = titleList.size
        var imgListSize = imgList.size
        var textListSize= textList.size
        // Récupération des listes de données

        if(dataListLength==1){
            while (authorListSize==0 || titleListSize==0 || imgListSize==0 || textListSize==0){
                authorList = SubredditPostGetter.getauthorList()
                titleList = SubredditPostGetter.gettitleList()
                imgList = SubredditPostGetter.getimgList()
                textList = SubredditPostGetter.gettextList()
                authorListSize = authorList.size
            }
        }

        while(authorListSize<dataListLength && titleListSize<dataListLength && imgListSize<dataListLength && textListSize<dataListLength){
            authorList = SubredditPostGetter.getauthorList()
            titleList = SubredditPostGetter.gettitleList()
            imgList = SubredditPostGetter.getimgList()
            textList = SubredditPostGetter.gettextList()

            authorListSize = authorList.size
            titleListSize = titleList.size
            imgListSize = imgList.size
            textListSize= textList.size
            dataListLength = SubredditPostGetter.SubredditPostListLenght
        }

        var i=0
        PostRedditList = emptyArray()
        while (i<authorList.size-1){
            var author = authorList[i].toString()
            var title = titleList[i].toString()
            var img = imgList[i].toString().replace("amp;","")
            //img = img
            var text = textList[i].toString()
            PostRedditList += PostReddit(author, title, img, text)
            i++
        }
        binding.listSubredditSub.adapter = PostAdapter(PostRedditList)
        binding.joinBtn.setOnClickListener{
            //view.findNavController().navigate(R.id.action_home_to_profil)
            if(binding.joinBtn.text == "Subscribe"){
                JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"sub")
                binding.joinBtn.text = "Unsubscribe"
            }else{
                JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"unsub")
                binding.joinBtn.text = "Subscribe"
            }
        }

        getQuerySearch.resetquery()



        binding.hotBtn.setOnClickListener{

            SubredditDataGetter.resetsubredditId()
            SubredditDataGetter.resetsubredditDescription()
            SubredditDataGetter.resetsubredditImage()
            SubredditDataGetter.resetsubredditImageBackgroud()
            SubredditDataGetter.resetsubredditName()
            SubredditDataGetter.resetsubredditSubscribers()
            SubredditDataGetter.resetsubredditTitle()
            SubredditDataGetter.resetuserSubscribed()
            SubredditPostGetter.resetSubredditPostListLenght()
            SubredditPostGetter.resetAuthorList()
            SubredditPostGetter.resetTitleList()
            SubredditPostGetter.resetImgList()
            SubredditPostGetter.resetTextList()

            var token = TockenGetter.getMyAccessToken().toString()
            SubredditDataGetter.getSubredditData(token,subredditGiven)

            var nameSubreddit = SubredditDataGetter.getsubredditName()
            while(nameSubreddit==""||nameSubreddit==null){
                nameSubreddit= SubredditDataGetter.getsubredditName()
            }
            println("nameSubreddit : "+nameSubreddit)
            binding.subredditNameTV.text=nameSubreddit

            var descSubreddit = SubredditDataGetter.getsubredditDescription()
            while(descSubreddit==""||descSubreddit==null){
                descSubreddit= SubredditDataGetter.getsubredditDescription()
            }
            binding.subDescTV.text=descSubreddit

            var fullNameSubreddit = SubredditDataGetter.getsubredditTitle()
            while(fullNameSubreddit==""||fullNameSubreddit==null){
                fullNameSubreddit= SubredditDataGetter.getsubredditTitle()
            }
            binding.subredditTitleTV.text = fullNameSubreddit

            var Suscribers = SubredditDataGetter.getsubredditSubscribers()
            while(Suscribers==""||Suscribers==null){
                Suscribers= SubredditDataGetter.getsubredditSubscribers()
            }
            binding.followersSubTV.text = Suscribers+" followers"

            if(SubredditDataGetter.getuserSubscribed()=="true"){
                binding.joinBtn.text = "Unsubscribe"
            }else{
                binding.joinBtn.text = "Subscribe"
            }

            var subredditImg = SubredditDataGetter.getsubredditImage()
            while(subredditImg==""||subredditImg==null){
                subredditImg= SubredditDataGetter.getsubredditImage()
            }

            var subredditBackground = SubredditDataGetter.getsubredditImageBackgroud()
            while(subredditBackground==""||subredditBackground==null){
                subredditBackground= SubredditDataGetter.getsubredditImageBackgroud()
            }

            Picasso.get().load(subredditImg.replace("amp;","")).into(binding.subPictureIV)
            Picasso.get().load(subredditBackground.replace("amp;","")).into(binding.subBackgroundIV)


            SubredditPostGetter.getSubredditPost(token, subredditGiven, "hot")
            var dataListLength = SubredditPostGetter.SubredditPostListLenght
            while(dataListLength==0){
                dataListLength = SubredditPostGetter.SubredditPostListLenght
            }

            var authorList = SubredditPostGetter.getauthorList()
            var titleList = SubredditPostGetter.gettitleList()
            var imgList = SubredditPostGetter.getimgList()
            var textList = SubredditPostGetter.gettextList()

            var authorListSize = authorList.size
            var titleListSize = titleList.size
            var imgListSize = imgList.size
            var textListSize= textList.size
            // Récupération des listes de données

            if(dataListLength==1){
                while (authorListSize==0 || titleListSize==0 || imgListSize==0 || textListSize==0){
                    authorList = SubredditPostGetter.getauthorList()
                    titleList = SubredditPostGetter.gettitleList()
                    imgList = SubredditPostGetter.getimgList()
                    textList = SubredditPostGetter.gettextList()
                    authorListSize = authorList.size
                }
            }

            while(authorListSize<dataListLength && titleListSize<dataListLength && imgListSize<dataListLength && textListSize<dataListLength){
                authorList = SubredditPostGetter.getauthorList()
                titleList = SubredditPostGetter.gettitleList()
                imgList = SubredditPostGetter.getimgList()
                textList = SubredditPostGetter.gettextList()

                authorListSize = authorList.size
                titleListSize = titleList.size
                imgListSize = imgList.size
                textListSize= textList.size
                dataListLength = SubredditPostGetter.SubredditPostListLenght
            }

            var i=0
            PostRedditList = emptyArray()
            while (i<authorList.size-1){
                var author = authorList[i].toString()
                var title = titleList[i].toString()
                var img = imgList[i].toString().replace("amp;","")
                //img = img
                var text = textList[i].toString()
                PostRedditList += PostReddit(author, title, img, text)
                i++
            }
            binding.listSubredditSub.adapter = PostAdapter(PostRedditList)
            binding.joinBtn.setOnClickListener{
                //view.findNavController().navigate(R.id.action_home_to_profil)
                if(binding.joinBtn.text == "Subscribe"){
                    JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"sub")
                    binding.joinBtn.text = "Unsubscribe"
                }else{
                    JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"unsub")
                    binding.joinBtn.text = "Subscribe"
                }
            }

        }

        binding.topBtn.setOnClickListener{

            SubredditDataGetter.resetsubredditId()
            SubredditDataGetter.resetsubredditDescription()
            SubredditDataGetter.resetsubredditImage()
            SubredditDataGetter.resetsubredditImageBackgroud()
            SubredditDataGetter.resetsubredditName()
            SubredditDataGetter.resetsubredditSubscribers()
            SubredditDataGetter.resetsubredditTitle()
            SubredditDataGetter.resetuserSubscribed()
            SubredditPostGetter.resetSubredditPostListLenght()
            SubredditPostGetter.resetAuthorList()
            SubredditPostGetter.resetTitleList()
            SubredditPostGetter.resetImgList()
            SubredditPostGetter.resetTextList()

            var token = TockenGetter.getMyAccessToken().toString()
            SubredditDataGetter.getSubredditData(token,subredditGiven)

            var nameSubreddit = SubredditDataGetter.getsubredditName()
            while(nameSubreddit==""||nameSubreddit==null){
                nameSubreddit= SubredditDataGetter.getsubredditName()
            }
            println("nameSubreddit : "+nameSubreddit)
            binding.subredditNameTV.text=nameSubreddit

            var descSubreddit = SubredditDataGetter.getsubredditDescription()
            while(descSubreddit==""||descSubreddit==null){
                descSubreddit= SubredditDataGetter.getsubredditDescription()
            }
            binding.subDescTV.text=descSubreddit

            var fullNameSubreddit = SubredditDataGetter.getsubredditTitle()
            while(fullNameSubreddit==""||fullNameSubreddit==null){
                fullNameSubreddit= SubredditDataGetter.getsubredditTitle()
            }
            binding.subredditTitleTV.text = fullNameSubreddit

            var Suscribers = SubredditDataGetter.getsubredditSubscribers()
            while(Suscribers==""||Suscribers==null){
                Suscribers= SubredditDataGetter.getsubredditSubscribers()
            }
            binding.followersSubTV.text = Suscribers+" followers"

            if(SubredditDataGetter.getuserSubscribed()=="true"){
                binding.joinBtn.text = "Unsubscribe"
            }else{
                binding.joinBtn.text = "Subscribe"
            }

            var subredditImg = SubredditDataGetter.getsubredditImage()
            while(subredditImg==""||subredditImg==null){
                subredditImg= SubredditDataGetter.getsubredditImage()
            }

            var subredditBackground = SubredditDataGetter.getsubredditImageBackgroud()
            while(subredditBackground==""||subredditBackground==null){
                subredditBackground= SubredditDataGetter.getsubredditImageBackgroud()
            }

            Picasso.get().load(subredditImg.replace("amp;","")).into(binding.subPictureIV)
            Picasso.get().load(subredditBackground.replace("amp;","")).into(binding.subBackgroundIV)


            SubredditPostGetter.getSubredditPost(token, subredditGiven, "top")
            var dataListLength = SubredditPostGetter.SubredditPostListLenght
            while(dataListLength==0){
                dataListLength = SubredditPostGetter.SubredditPostListLenght
            }

            var authorList = SubredditPostGetter.getauthorList()
            var titleList = SubredditPostGetter.gettitleList()
            var imgList = SubredditPostGetter.getimgList()
            var textList = SubredditPostGetter.gettextList()

            var authorListSize = authorList.size
            var titleListSize = titleList.size
            var imgListSize = imgList.size
            var textListSize= textList.size
            // Récupération des listes de données

            if(dataListLength==1){
                while (authorListSize==0 || titleListSize==0 || imgListSize==0 || textListSize==0){
                    authorList = SubredditPostGetter.getauthorList()
                    titleList = SubredditPostGetter.gettitleList()
                    imgList = SubredditPostGetter.getimgList()
                    textList = SubredditPostGetter.gettextList()
                    authorListSize = authorList.size
                }
            }

            while(authorListSize<dataListLength && titleListSize<dataListLength && imgListSize<dataListLength && textListSize<dataListLength){
                authorList = SubredditPostGetter.getauthorList()
                titleList = SubredditPostGetter.gettitleList()
                imgList = SubredditPostGetter.getimgList()
                textList = SubredditPostGetter.gettextList()

                authorListSize = authorList.size
                titleListSize = titleList.size
                imgListSize = imgList.size
                textListSize= textList.size
                dataListLength = SubredditPostGetter.SubredditPostListLenght
            }

            var i=0
            PostRedditList = emptyArray()
            while (i<authorList.size-1){
                var author = authorList[i].toString()
                var title = titleList[i].toString()
                var img = imgList[i].toString().replace("amp;","")
                //img = img
                var text = textList[i].toString()
                PostRedditList += PostReddit(author, title, img, text)
                i++
            }
            binding.listSubredditSub.adapter = PostAdapter(PostRedditList)
            binding.joinBtn.setOnClickListener{
                //view.findNavController().navigate(R.id.action_home_to_profil)
                if(binding.joinBtn.text == "Subscribe"){
                    JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"sub")
                    binding.joinBtn.text = "Unsubscribe"
                }else{
                    JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"unsub")
                    binding.joinBtn.text = "Subscribe"
                }
            }
        }

        binding.newBtn.setOnClickListener{

            SubredditDataGetter.resetsubredditId()
            SubredditDataGetter.resetsubredditDescription()
            SubredditDataGetter.resetsubredditImage()
            SubredditDataGetter.resetsubredditImageBackgroud()
            SubredditDataGetter.resetsubredditName()
            SubredditDataGetter.resetsubredditSubscribers()
            SubredditDataGetter.resetsubredditTitle()
            SubredditDataGetter.resetuserSubscribed()
            SubredditPostGetter.resetSubredditPostListLenght()
            SubredditPostGetter.resetAuthorList()
            SubredditPostGetter.resetTitleList()
            SubredditPostGetter.resetImgList()
            SubredditPostGetter.resetTextList()

            var token = TockenGetter.getMyAccessToken().toString()
            SubredditDataGetter.getSubredditData(token,subredditGiven)

            var nameSubreddit = SubredditDataGetter.getsubredditName()
            while(nameSubreddit==""||nameSubreddit==null){
                nameSubreddit= SubredditDataGetter.getsubredditName()
            }
            println("nameSubreddit : "+nameSubreddit)
            binding.subredditNameTV.text=nameSubreddit

            var descSubreddit = SubredditDataGetter.getsubredditDescription()
            while(descSubreddit==""||descSubreddit==null){
                descSubreddit= SubredditDataGetter.getsubredditDescription()
            }
            binding.subDescTV.text=descSubreddit

            var fullNameSubreddit = SubredditDataGetter.getsubredditTitle()
            while(fullNameSubreddit==""||fullNameSubreddit==null){
                fullNameSubreddit= SubredditDataGetter.getsubredditTitle()
            }
            binding.subredditTitleTV.text = fullNameSubreddit

            var Suscribers = SubredditDataGetter.getsubredditSubscribers()
            while(Suscribers==""||Suscribers==null){
                Suscribers= SubredditDataGetter.getsubredditSubscribers()
            }
            binding.followersSubTV.text = Suscribers+" followers"

            if(SubredditDataGetter.getuserSubscribed()=="true"){
                binding.joinBtn.text = "Unsubscribe"
            }else{
                binding.joinBtn.text = "Subscribe"
            }

            var subredditImg = SubredditDataGetter.getsubredditImage()
            while(subredditImg==""||subredditImg==null){
                subredditImg= SubredditDataGetter.getsubredditImage()
            }

            var subredditBackground = SubredditDataGetter.getsubredditImageBackgroud()
            while(subredditBackground==""||subredditBackground==null){
                subredditBackground= SubredditDataGetter.getsubredditImageBackgroud()
            }

            Picasso.get().load(subredditImg.replace("amp;","")).into(binding.subPictureIV)
            Picasso.get().load(subredditBackground.replace("amp;","")).into(binding.subBackgroundIV)


            SubredditPostGetter.getSubredditPost(token, subredditGiven, "new")
            var dataListLength = SubredditPostGetter.SubredditPostListLenght
            while(dataListLength==0){
                dataListLength = SubredditPostGetter.SubredditPostListLenght
            }

            var authorList = SubredditPostGetter.getauthorList()
            var titleList = SubredditPostGetter.gettitleList()
            var imgList = SubredditPostGetter.getimgList()
            var textList = SubredditPostGetter.gettextList()

            var authorListSize = authorList.size
            var titleListSize = titleList.size
            var imgListSize = imgList.size
            var textListSize= textList.size
            // Récupération des listes de données

            if(dataListLength==1){
                while (authorListSize==0 || titleListSize==0 || imgListSize==0 || textListSize==0){
                    authorList = SubredditPostGetter.getauthorList()
                    titleList = SubredditPostGetter.gettitleList()
                    imgList = SubredditPostGetter.getimgList()
                    textList = SubredditPostGetter.gettextList()
                    authorListSize = authorList.size
                }
            }

            while(authorListSize<dataListLength && titleListSize<dataListLength && imgListSize<dataListLength && textListSize<dataListLength){
                authorList = SubredditPostGetter.getauthorList()
                titleList = SubredditPostGetter.gettitleList()
                imgList = SubredditPostGetter.getimgList()
                textList = SubredditPostGetter.gettextList()

                authorListSize = authorList.size
                titleListSize = titleList.size
                imgListSize = imgList.size
                textListSize= textList.size
                dataListLength = SubredditPostGetter.SubredditPostListLenght
            }

            var i=0
            PostRedditList = emptyArray()
            while (i<authorList.size-1){
                var author = authorList[i].toString()
                var title = titleList[i].toString()
                var img = imgList[i].toString().replace("amp;","")
                //img = img
                var text = textList[i].toString()
                PostRedditList += PostReddit(author, title, img, text)
                i++
            }
            binding.listSubredditSub.adapter = PostAdapter(PostRedditList)
            binding.joinBtn.setOnClickListener{
                //view.findNavController().navigate(R.id.action_home_to_profil)
                if(binding.joinBtn.text == "Subscribe"){
                    JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"sub")
                    binding.joinBtn.text = "Unsubscribe"
                }else{
                    JoinLeaveSubreddit.JoinLeaveSubreddit(token,subredditGiven,"unsub")
                    binding.joinBtn.text = "Subscribe"
                }
            }
        }

    }
}