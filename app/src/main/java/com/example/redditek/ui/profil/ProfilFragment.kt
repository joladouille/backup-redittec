package com.example.redditek.ui.profil

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.redditek.ProfileGetter
import com.example.redditek.R
import com.example.redditek.SubredditDataGetter
import com.example.redditek.TockenGetter
import com.example.redditek.databinding.FragmentProfilBinding
import com.squareup.picasso.Picasso

class ProfilFragment : Fragment() {

    private lateinit var binding: FragmentProfilBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfilBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.editButton.setOnClickListener{
            view.findNavController().navigate(R.id.action_profils_to_edit_profils)
        }
        var token = TockenGetter.getMyAccessToken()

        ProfileGetter.getProfile(token)

        var description = ProfileGetter.getMyUserDescription()
        while(description==""||description==null){
            description= ProfileGetter.getMyUserDescription()
        }

        var name = ProfileGetter.getMyUserName()
        while(name==""||name==null){
            name= ProfileGetter.getMyUserName()
        }

        var imageUser = ProfileGetter.getMyUserImage()
        while(imageUser==""||imageUser==null){
            imageUser= ProfileGetter.getMyUserImage()
        }

        binding.nameTV.text = name

        binding.profilDesciptionTV.text = description

        val profilPicture : ImageView = view.findViewById(R.id.profil_pickture_IV)

        Picasso.get().load(imageUser.replace("amp;","")).into(profilPicture)


    }
}