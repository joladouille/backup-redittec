package com.example.redditek.ui.home

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.redditek.R
import com.example.redditek.SubredditPostGetter
import com.example.redditek.SubredditPostGetter.Companion.authorList
import com.example.redditek.SubredditPostGetter.Companion.getSubredditPost
import com.example.redditek.TockenGetter.Companion.getMyAccessToken
import com.example.redditek.adapter.PostAdapter
import com.example.redditek.databinding.FragmentHomeBinding
import com.example.redditek.dommain.PostReddit
import com.example.redditek.getQuerySearch

class HomeFragment : Fragment(){

    private lateinit var binding: FragmentHomeBinding

    //TODO: remove test
    private var PostRedditList: Array<PostReddit> = Array(1) {PostReddit("","", "","")}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        binding.listSubredditAccueil.layoutManager = LinearLayoutManager(activity)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var token = getMyAccessToken().toString()
        println("Token Home : "+token)

        getSubredditPost(token,"StrikeForceHeroes","hot")
        var dataListLength = SubredditPostGetter.SubredditPostListLenght
        while(dataListLength==0){
            dataListLength = SubredditPostGetter.SubredditPostListLenght
        }
        println("dataListLength : "+ dataListLength)
        var authorList = SubredditPostGetter.getauthorList()
        var titleList = SubredditPostGetter.gettitleList()
        var imgList = SubredditPostGetter.getimgList()
        var textList = SubredditPostGetter.gettextList()

        var authorListSize = authorList.size
        var titleListSize = titleList.size
        var imgListSize = imgList.size
        var textListSize= textList.size
        // Récupération des listes de données

        if(dataListLength==1){
            while (authorListSize==0 || titleListSize==0 || imgListSize==0 || textListSize==0){
                authorList = SubredditPostGetter.getauthorList()
                titleList = SubredditPostGetter.gettitleList()
                imgList = SubredditPostGetter.getimgList()
                textList = SubredditPostGetter.gettextList()
                authorListSize = authorList.size
            }
        }

        while(authorListSize<dataListLength && titleListSize<dataListLength && imgListSize<dataListLength && textListSize<dataListLength){
            authorList = SubredditPostGetter.getauthorList()
            titleList = SubredditPostGetter.gettitleList()
            imgList = SubredditPostGetter.getimgList()
            textList = SubredditPostGetter.gettextList()

            authorListSize = authorList.size
            titleListSize = titleList.size
            imgListSize = imgList.size
            textListSize= textList.size
            dataListLength = SubredditPostGetter.SubredditPostListLenght
        }

        var i=0
        while (i<authorList.size-1){
            var author = authorList[i].toString()
            var title = titleList[i].toString()
            var img = imgList[i].toString().replace("amp;","")
            //img = img
            var text = textList[i].toString()
            if(i==0){
                PostRedditList[0] = PostReddit(author, title, img, text)
            }else{
                PostRedditList += PostReddit(author, title, img, text)
            }
            i++
        }
        binding.listSubredditAccueil.adapter = PostAdapter(PostRedditList)


        val search = binding.searchView
        searching(search)
    }

    private fun searching(search: SearchView) {
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                println("Submit : " + query)
                getQuerySearch.setquery(query.toString())
                search.findNavController().navigate(R.id.action_home_to_subReddit)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.i(TAG,"Change")
                return true
            }
        })
    }

}