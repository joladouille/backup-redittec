package com.example.redditek

import android.content.ContentValues
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Button
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class TockenGetter {

    val AUTH_URL = "https://www.reddit.com/api/v1/authorize.compact?client_id=%s" +
            "&response_type=code&state=%s&redirect_uri=%s&" +
            "duration=permanent&scope=identity"

    val CLIENT_ID = "mFgWyX5FA03ozeTfIqGNgQ"

    val REDIRECT_URI = "http://www.testingoauth2.com/my_redirect"

    val STATE = "MY_RANDOM_STRING_1"

    val ACCESS_TOKEN_URL = "https://www.reddit.com/api/v1/access_token"


    companion object {

        var accessToken : String = ""
        protected var refreshToken : String = ""

        fun getMyAccessToken(): String {
            val sortie =  this.accessToken
            return sortie
        }

        fun getMyRefreshToken(): String {
            val sortie =  this.refreshToken
            return sortie
        }

        fun getAccessToken(code: String?) {
            val client = OkHttpClient()
            val authString = "mFgWyX5FA03ozeTfIqGNgQ:"
            val encodedAuthString: String = Base64.encodeToString(
                authString.toByteArray(),
                Base64.NO_WRAP
            )

            val request: Request = Request.Builder()
                .addHeader("User-Agent", "android:com.example.redditek:v1 (by /u/gsorinEpitech)")
                .addHeader("Authorization", "Basic $encodedAuthString")
                .url("https://www.reddit.com/api/v1/access_token")
                .post(
                    RequestBody.create(
                        MediaType.parse("application/x-www-form-urlencoded"),
                        "grant_type=authorization_code&code=" + code +
                                "&redirect_uri=http://www.testingoauth2.com/my_redirect"
                    )
                )
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call?, e: IOException) {
                    Log.e(ContentValues.TAG, "ERROR: $e")
                }

                @Throws(IOException::class)
                override fun onResponse(call: Call?, response: Response) {
                    val json = response.body()?.string()
                    var data: JSONObject? = null
                    try {
                        data = JSONObject(json)
                        println(data)
                        accessToken = data.optString("access_token")
                        refreshToken= data.optString("refresh_token")
                        //val refreshToken = data.optString("refresh_token")
                        // return ces valeurs)
                        Log.d(ContentValues.TAG, "data token = $data")
                        Log.d(ContentValues.TAG, "Access Token = $accessToken")
                        Log.d(ContentValues.TAG, "Refresh Token = $refreshToken")

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            })
        }
    }

}