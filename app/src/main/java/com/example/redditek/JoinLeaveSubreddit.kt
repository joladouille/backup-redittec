package com.example.redditek

import okhttp3.*
import org.json.JSONObject
import java.io.IOException


class JoinLeaveSubreddit {

    companion object{

        fun JoinLeaveSubreddit(token:String,subredditGiven:String,actionGiven:String) {
            println("datafunc : $subredditGiven , $actionGiven")

            val client = OkHttpClient()
            val request = Request.Builder()
                .addHeader("User-Agent", "android:com.example.redditek:v1 (by /u/gsorinEpitech)")
                .addHeader("Authorization","Bearer "+token)
                .addHeader("Content_Type","application/x-www-form-urlencoded")
                .url("https://oauth.reddit.com/api/subscribe")
                .post(
                    RequestBody.create(
                        MediaType.parse("application/x-www-form-urlencoded"),
                        "sr_name="+subredditGiven+"&action="+actionGiven
                    )
                )
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response){

                    println("reponse Subscribing : "+response)
                    println("Subscribing : "+ JSONObject(response.body()?.string().toString()))


                }
            })


        }
    }
}