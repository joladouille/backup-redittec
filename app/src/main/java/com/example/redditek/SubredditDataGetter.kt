package com.example.redditek

import android.content.ContentValues
import android.util.Log
import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class SubredditDataGetter {

    companion object{

        protected var subredditName : String = ""
        protected var subredditId : String = ""
        protected var subredditTitle : String = ""
        protected var subredditDescription : String = ""
        protected var subredditImage : String = ""
        protected var subredditImageBackgroud : String = ""
        protected var userSubscribed : String = ""
        protected var subredditSubscribers :String = ""

        fun getsubredditName(): String{
            val sortie =  this.subredditName
            return sortie
        }
        fun resetsubredditName(){
            this.subredditName = ""
        }

        fun getsubredditId(): String{
            val sortie =  this.subredditId
            return sortie
        }
        fun resetsubredditId(){
            this.subredditId = ""
        }

        fun getsubredditTitle(): String{
            val sortie =  this.subredditTitle
            return sortie
        }
        fun resetsubredditTitle(){
            this.subredditTitle = ""
        }

        fun getsubredditDescription(): String{
            val sortie =  this.subredditDescription
            return sortie
        }
        fun resetsubredditDescription(){
            this.subredditDescription = ""
        }

        fun getsubredditImage(): String{
            val sortie =  this.subredditImage
            return sortie
        }
        fun resetsubredditImage(){
            this.subredditImage = ""
        }

        fun getsubredditImageBackgroud(): String{
            val sortie =  this.subredditImageBackgroud
            return sortie
        }
        fun resetsubredditImageBackgroud(){
            this.subredditImageBackgroud = ""
        }

        fun getuserSubscribed(): String{
            val sortie =  this.userSubscribed
            return sortie
        }
        fun resetuserSubscribed(){
            this.userSubscribed = ""
        }

        fun getsubredditSubscribers(): String{
            val sortie =  this.subredditSubscribers
            return sortie
        }
        fun resetsubredditSubscribers(){
            this.subredditSubscribers = ""
        }


        fun getSubredditData(token:String,subredditGiven:String) {
            val client = OkHttpClient()
            val request = Request.Builder()
                .addHeader("User-Agent", "android:com.example.redditek:v1 (by /u/gsorinEpitech)")
                .addHeader("Authorization","Bearer "+token)
                .url("https://oauth.reddit.com/r/"+subredditGiven+"/about")
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response){
                    val jsonObject = JSONObject(response.body()?.string().toString())
                    println("SubredditData : "+jsonObject)

                    var moreData = jsonObject.getString("data")
                    val jsonObjectMoreData = JSONObject(moreData)

                    var gettingTitleSubreddit =jsonObjectMoreData.getString("title")

                    var gettingNameSubreddit =jsonObjectMoreData.getString("display_name")

                    var gettingIdSubreddit =jsonObjectMoreData.getString("name")

                    var gettingDescriptionSubreddit =jsonObjectMoreData.getString("public_description")

                    var gettingImgSubreddit =jsonObjectMoreData.getString("community_icon") // banner_background_image ? or header_img ?

                    var gettingHeaderImgSubreddit =jsonObjectMoreData.getString("banner_background_image") // banner_background_image ? or header_img ?

                    if(gettingHeaderImgSubreddit==""){
                        gettingHeaderImgSubreddit="empty"
                    }

                    var gettingUserIsSubscriber =jsonObjectMoreData.getString("user_is_subscriber")

                    var gettingSubscribers =jsonObjectMoreData.getString("subscribers")


                    subredditName  = gettingNameSubreddit
                    subredditId = gettingIdSubreddit
                    subredditTitle  = gettingTitleSubreddit
                    subredditDescription  = gettingDescriptionSubreddit
                    subredditImage  = gettingImgSubreddit
                    subredditImageBackgroud = gettingHeaderImgSubreddit
                    userSubscribed = gettingUserIsSubscriber
                    subredditSubscribers = gettingSubscribers

                    println("subreddit name : "+subredditName)
                    println("subreddit subscribers : "+gettingSubscribers)
                }
            })
        }
    }
}