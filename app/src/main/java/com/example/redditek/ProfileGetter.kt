package com.example.redditek

import okhttp3.*
import org.json.JSONObject
import java.io.IOException

class ProfileGetter {

    companion object{

        protected var userName : String = ""
        protected var userDescription : String = ""
        protected var userImage : String = ""

        fun getMyUserName(): String{
            val sortie =  this.userName
            return sortie
        }

        fun getMyUserDescription(): String{
            val sortie =  this.userDescription
            return sortie
        }

        fun getMyUserImage(): String{
            val sortie =  this.userImage
            return sortie
        }

        fun getProfile(token:String) {
            val client = OkHttpClient()
            val request = Request.Builder()
                .addHeader("User-Agent", "android:com.example.redditek:v1 (by /u/gsorinEpitech)")
                .addHeader("Authorization","Bearer "+token)
                .url("https://oauth.reddit.com/api/v1/me")
                .build()

            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {}
                override fun onResponse(call: Call, response: Response){
                    val jsonObject = JSONObject(response.body()?.string().toString())

                    var moreData = jsonObject.getString("subreddit")
                    val jsonObjectMoreData =JSONObject(moreData)

                    //Récupération du nom du User
                    var gettingUserName =jsonObject.getString("name")
                    println("userName :"+gettingUserName)
                    //Récupération de la description
                    var gettingUserDesc =jsonObjectMoreData.getString("public_description")
                    println("userDescription :"+gettingUserDesc)
                    // Récupération de l'icone
                    var gettingUserIcon =jsonObjectMoreData.getString("icon_img")
                    println("userImage :"+gettingUserIcon)

                    userName=gettingUserName
                    userDescription=gettingUserDesc
                    userImage=gettingUserIcon
                }
            })
        }
    }
}
