package com.example.redditek.dommain

data class PostReddit(
    val author:String,
    val title:String,
    val img:String,
    val text:String
) {
}